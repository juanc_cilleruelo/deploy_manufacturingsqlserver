Contenido del directorio [senCilleERP]                                 
---------------------------------------------------------------      
                                                                      
 Copyright 1996-2016 by                                               
  senCille Software (Juan Carlos Cilleruelo)                          
                                                                      
   http://sencille.es                                                 
                                                                      
                                                                   
Bit�cora de versiones.                                 
======================================                                
                                                                      
   Versi�n inicial del programa instalada para Industrias Rodrigo S.A.                                         
                                                                      
   En cada punto se describen las novedades, correcciones y mejoras
   introducidas con la nueva versi�n.                                 
                                                                      
     � Copia inicial del programa.                    
                                                                      
     �                 
                                                                      
     �                                                      
                                                                      
 Contacte con nosotros                                                
 ---------------------                                                
                                                                      
     � soporte@sencille.es                                            
                                                                      
     � Tel�fono: 983 880 970                                          
                                                                      
     � Calle de las Tercias 19                                        
       47300 PE�AFIEL (Valladolid) SPAIN                              
                                                                      
     � http://sencille.es                                             
                                                                      
                                                                      
